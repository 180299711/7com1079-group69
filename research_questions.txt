 Group: group 69
    
 Question
 ========
    
 RQ: Is there a difference in the mean of indicators between Less Developed Countries and More Developed countries?
    
 Null hypothesis: There is no difference in the mean of indicators between Less Developed Countries and More Developed countries.
    
 Alternative hypothesis: There is a difference in the mean of indicators between Less Developed Countries and More Developed countries.
    
 Dataset
 =======
    
 URL: https://www.kaggle.com/worldbank/world-development-indicators?select=Indicators.csv
    
 Column Headings:

 ```
 > View(final_indicator)
 > colnames(final_indicator)
	[1] "Year"          "CountryCode"   "CountryName"   "Region"        "Category"      "IncomeGroup"   "IndicatorName"
	[8] "Value"

 ```
   
